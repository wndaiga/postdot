import express from "express";
import dotenv from "dotenv";
import cors from "cors";

dotenv.config();

const PORT = process.env.PORT;

const app = express();

app.use(cors());

app.get("/api/v1", (req, res) => {
  res.send({
    message: "postdot API",
  });
});

app.listen(PORT, () => {
  console.log(`📭[postdot] Server runnning on port ${PORT}`);
});
