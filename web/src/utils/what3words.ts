interface w3wFetchOpts {
  method: "HEAD" | "GET" | "POST" | "PUT" | "DELETE" | "OPTIONS";
  headers: {
    Accepts: string;
    "Content-Type": string;
  };
  body: any;
  query: any;
}

const WHAT3WORDS_API_KEY: string =
  process.env.REACT_APP_WHAT3WORDS_API_KEY || "";
const WHAT3WORDS_API_BASE_URL: string =
  process.env.REACT_APP_WHAT3WORDS_API_BASE_URL || "";

export const w3wFetch = async (
  resourceUri: string,
  { query, method = "GET", headers, body }: Partial<w3wFetchOpts> = {}
): Promise<Response> => {
  const searchParams = new URLSearchParams({
    ...query,
    key: WHAT3WORDS_API_KEY,
  });
  // use short circuit evaluations to conditionally spread headers and body into opts object
  const opts = {
    method,
    ...(headers && {
      headers: {
        ...headers,
      },
    }),
    ...(body && {
      body: {
        ...body,
      },
    }),
  };

  return await fetch(
    `${WHAT3WORDS_API_BASE_URL}${resourceUri}?${searchParams}`,
    opts
  );
};
