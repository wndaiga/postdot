import React from "react";
import mapboxgl from "mapbox-gl";
import "./App.css";
import { w3wFetch } from "./utils";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface formInputSchema {
  wordsSearch?: string;
}

interface w3wAutoSuggestion {
  country: string;
  nearestPlace: string;
  words: string;
  rank: number;
  language: string;
}

const MAPBOX_ACCESS_TOKEN = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

const Map = () => {
  const mapContainer = React.useRef<HTMLDivElement>(null);
  const [map, setMap] = React.useState<mapboxgl.Map | null>(null);

  React.useEffect(() => {
    mapboxgl.accessToken = `${MAPBOX_ACCESS_TOKEN}`;
    const tmpMap = new mapboxgl.Map({
      container: "map",
      style: "mapbox://styles/mapbox/light-v10",
      center: [36.817223, -1.286389],
      zoom: 5,
    });
    setMap(tmpMap);
  }, []);

  return (
    <div
      id="map"
      ref={mapContainer}
      style={{ width: "100vw", height: "100vh" }}
    />
  );
};

function App() {
  const [formData, setFormData] = React.useState<formInputSchema>({});
  const [w3wAutoSuggestions, setW3wAutoSuggestions] = React.useState<
    w3wAutoSuggestion[]
  >([]);

  const onInputChange = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();
    const target = e.target as HTMLInputElement;

    if (target.name === "wordsSearch") {
      const w3wResp = await w3wFetch("/autosuggest", {
        query: {
          input: target.value,
        },
      });

      if (!w3wResp.ok) {
        toast.error("An error occurred while retrieving suggestions");
        return;
      }

      const { suggestions } = await w3wResp.json();

      setW3wAutoSuggestions(suggestions);
    }

    setFormData({ ...formData, [target.name]: target.value });
  };

  return (
    <div className="app">
      <header className="app-header">
        <h1>postdot</h1>
        <input
          className="words-search-input"
          type="text"
          name="wordsSearch"
          placeholder="///hello.there.stranger"
          onChange={onInputChange}
        ></input>
      </header>
      {!!w3wAutoSuggestions.length && (
        <div className="words-suggestions-container">
          <ul className="words-suggestions-list">
            {w3wAutoSuggestions.map((suggestion: w3wAutoSuggestion, idx) => (
              <li
                key={`words-suggestion-item-${suggestion.nearestPlace}-${idx}`}
              >
                {suggestion.nearestPlace}
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="app-map-container">
        <Map />
      </div>
      <ToastContainer
        autoClose={5000}
        closeOnClick
        hideProgressBar
        position="bottom-right"
      />
    </div>
  );
}

export default App;
